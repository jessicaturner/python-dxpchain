# -*- coding: utf-8 -*-
from dxpchain.utils import assets_from_string


def test_assets_from_string():
    assert assets_from_string("USD:DXP") == ["USD", "DXP"]
    assert assets_from_string("DXPBOTS.S1:DXP") == ["DXPBOTS.S1", "DXP"]
