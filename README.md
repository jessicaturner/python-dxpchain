## Documentation

Visit the [pydxpchain website](http://docs.pydxpchain.com/en/latest/) for in depth documentation on this Python library.

## Installation

### Install with pip3:

    $ sudo apt-get install libffi-dev libssl-dev python-dev python3-dev python3-pip

### Manual installation:

    $ git clone https://gitlab.com/jessicaturner/python-graphenelib/
    $ cd python-graphenelib
    $ python3 setup.py install --user

    $ git clone https://gitlab.com/jessicaturner/python-dxpchain
    $ cd python-dxpchain
    $ python3 setup.py install --user

## Contributing

python-dxpchain welcomes contributions from anyone and everyone. Please
see our [guidelines for contributing](CONTRIBUTING.md) and the [code of
conduct](CODE_OF_CONDUCT.md).

### Discussion and Developers

Discussions around development and use of this library can be found in a
[dedicated Telegram Channel](https://t.me/pydxpchain)

### License

A copy of the license is available in the repository's
[LICENSE](LICENSE.txt) file.
