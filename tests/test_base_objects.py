# -*- coding: utf-8 -*-
import unittest
from dxpchain import DxpChain, exceptions
from dxpchain.instance import set_shared_dxpchain_instance
from dxpchain.account import Account
from dxpchain.dxpcore import Dxpcore
from .fixtures import fixture_data


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_Dxpcore(self):
        with self.assertRaises(exceptions.AccountDoesNotExistsException):
            Dxpcore("FOObarNonExisting")

        c = Dxpcore("init0")
        self.assertEqual(c["id"], "1.5.27")
        self.assertIsInstance(c.account, Account)

        with self.assertRaises(exceptions.DxpcoreMemberDoesNotExistsException):
            Dxpcore("matrix")
