# -*- coding: utf-8 -*-
from .account import Account
from .instance import BlockchainInstance
from graphenecommon.aio.dxpcore import Dxpcore as GrapheneDxpcore


@BlockchainInstance.inject
class Dxpcore(GrapheneDxpcore):
    """
    Read data about a Dxpcore Member in the chain.

    :param str member: Name of the Dxpcore Member
    :param dxpchain blockchain_instance: DxpChain() instance to use when
        accesing a RPC
    :param bool lazy: Use lazy loading
    """

    def define_classes(self):
        self.type_id = 5
        self.account_class = Account
