# -*- coding: utf-8 -*-
import asyncio
import pytest
import logging

from dxpchain.aio.message import Message

log = logging.getLogger("grapheneapi")
log.setLevel(logging.DEBUG)


@pytest.mark.asyncio
async def test_sign_message(dxpchain, default_account):
    message = await Message("message foobar", blockchain_instance=dxpchain)
    p = await message.sign(account=default_account)
    m = await Message(p, blockchain_instance=dxpchain)
    await m.verify()
