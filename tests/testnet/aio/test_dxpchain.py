# -*- coding: utf-8 -*-
import asyncio
import pytest
import logging

from datetime import datetime
from dxpchain.aio.asset import Asset
from dxpchain.aio.amount import Amount
from dxpchain.aio.account import Account
from dxpchain.aio.price import Price
from dxpchain.aio.proposal import Proposals
from dxpchain.aio.benefactor import Benefactors
from dxpchain.aio.dex import Dex
from dxpchain.aio.market import Market

log = logging.getLogger("grapheneapi")
log.setLevel(logging.DEBUG)


@pytest.fixture(scope="module")
async def testbenefactor(dxpchain, default_account):
    amount = await Amount("1000 TEST")
    end = datetime(2099, 1, 1)
    await dxpchain.create_benefactor("test", amount, end, account=default_account)


@pytest.fixture(scope="module")
async def gs_smarttoken(dxpchain, default_account, base_smarttoken):
    """Create globally settled smarttoken."""
    asset = await base_smarttoken()

    price = await Price(10.0, base=asset, quote=await Asset("TEST"))
    await dxpchain.publish_price_feed(asset.symbol, price, account=default_account)
    dex = Dex(blockchain_instance=dxpchain)
    to_borrow = await Amount(1000, asset)
    await dex.borrow(to_borrow, collateral_ratio=2.1, account=default_account)
    price = await Price(1.0, base=asset, quote=await Asset("TEST"))
    # Trigger GS
    await dxpchain.publish_price_feed(asset.symbol, price, account=default_account)
    return asset


@pytest.fixture(scope="module")
async def ltm_account(dxpchain, default_account, unused_account):
    account = await unused_account()
    await dxpchain.create_account(
        account, referrer=default_account, registrar=default_account, password="test"
    )
    await dxpchain.transfer(
        account, 100000, "TEST", memo="xxx", account=default_account
    )
    await dxpchain.upgrade_account(account=account)
    return account


@pytest.mark.asyncio
async def test_aio_chain_props(dxpchain):
    """Test chain properties."""
    # Wait for several blcocks
    await asyncio.sleep(3)
    props = await dxpchain.info()
    assert isinstance(props, dict)
    assert props["head_block_number"] > 0


@pytest.mark.asyncio
async def test_transfer(dxpchain, default_account):
    await dxpchain.transfer("init1", 10, "TEST", memo="xxx", account=default_account)


@pytest.mark.asyncio
async def test_create_account(dxpchain, default_account):
    await dxpchain.create_account(
        "foobar", referrer=default_account, registrar=default_account, password="test"
    )


@pytest.mark.asyncio
async def test_upgrade_account(ltm_account):
    account = await Account(ltm_account)
    assert account.is_ltm


@pytest.mark.asyncio
async def test_allow_disallow(dxpchain, default_account):
    await dxpchain.allow("init1", account=default_account)
    await asyncio.sleep(1.1)
    await dxpchain.disallow("init1", account=default_account)


@pytest.mark.asyncio
async def test_update_memo_key(dxpchain, ltm_account, default_account):
    from dxpchainbase.account import PasswordKey

    account = ltm_account
    password = "test2"
    memo_key = PasswordKey(account, password, role="memo")
    pubkey = memo_key.get_public_key()
    await dxpchain.update_memo_key(pubkey, account=account)


@pytest.mark.asyncio
async def test_approve_disapprove_blockproducer(dxpchain, default_account):
    blockproducers = ["init1", "init2"]
    await dxpchain.approveblockproducer(blockproducers, account=default_account)
    await asyncio.sleep(1.1)
    await dxpchain.disapproveblockproducer(blockproducers, account=default_account)


@pytest.mark.asyncio
async def test_approve_disapprove_dxpcore(dxpchain, default_account):
    cm = ["init5", "init6"]
    await dxpchain.approvedxpcore(cm, account=default_account)
    await asyncio.sleep(1.1)
    await dxpchain.disapprovedxpcore(cm, account=default_account)


@pytest.mark.asyncio
async def test_approve_proposal(dxpchain, default_account):
    # Set blocking to get "operation_results"
    dxpchain.blocking = "head"
    proposal = dxpchain.new_proposal()
    await dxpchain.transfer(
        "init1", 1, "TEST", append_to=proposal, account=default_account
    )
    tx = await proposal.broadcast()
    proposal_id = tx["operation_results"][0][1]
    await dxpchain.approveproposal(proposal_id, account=default_account)
    dxpchain.blocking = None


@pytest.mark.asyncio
async def test_disapprove_proposal(dxpchain, default_account, unused_account):
    # Create child account
    account = await unused_account()
    await dxpchain.create_account(
        account, referrer=default_account, registrar=default_account, password="test"
    )
    await dxpchain.transfer(account, 100, "TEST", account=default_account)

    # Grant child account access with 1/2 threshold
    await dxpchain.allow(account, weight=1, threshold=2, account=default_account)

    # Create proposal
    dxpchain.blocking = "head"
    proposal = dxpchain.new_proposal()
    await dxpchain.transfer(
        "init1", 1, "TEST", append_to=proposal, account=default_account
    )
    tx = await proposal.broadcast()
    proposal_id = tx["operation_results"][0][1]

    # Approve proposal; 1/2 is not sufficient to completely approve, so proposal remains active
    await dxpchain.approveproposal(proposal_id, account=account)
    # Revoke vote
    await dxpchain.disapproveproposal(proposal_id, account=account)
    dxpchain.blocking = None


@pytest.mark.asyncio
async def test_approve_disapprove_benefactor(dxpchain, testbenefactor, default_account):
    benefactors = await Benefactors(default_account)
    benefactor = benefactors[0]["id"]
    await dxpchain.approvebenefactor(benefactor)
    await dxpchain.disapprovebenefactor(benefactor)


@pytest.mark.asyncio
async def test_set_unset_proxy(dxpchain, default_account):
    await dxpchain.set_proxy("init1", account=default_account)
    await asyncio.sleep(1.1)
    await dxpchain.unset_proxy()


@pytest.mark.skip(reason="cancel() tested indirectly in test_market.py")
@pytest.mark.asyncio
async def test_cancel():
    pass


@pytest.mark.skip(reason="need to provide a way to make non-empty vesting balance")
@pytest.mark.asyncio
async def test_vesting_balance_withdraw(dxpchain, default_account):
    balances = await dxpchain.rpc.get_vesting_balances(default_account)
    await dxpchain.vesting_balance_withdraw(balances[0]["id"], account=default_account)


@pytest.mark.asyncio
async def test_publish_price_feed(dxpchain, base_smarttoken, default_account):
    asset = await base_smarttoken()
    price = await Price(1.1, base=asset, quote=await Asset("TEST"))
    await dxpchain.publish_price_feed(asset.symbol, price, account=default_account)


@pytest.mark.asyncio
async def test_update_cer(dxpchain, base_smarttoken, default_account):
    asset = await base_smarttoken()
    price = await Price(1.2, base=asset, quote=await Asset("TEST"))
    await dxpchain.update_cer(asset.symbol, price, account=default_account)


@pytest.mark.asyncio
async def test_update_blockproducer(dxpchain, default_account):
    await dxpchain.update_blockproducer(default_account, url="https://foo.bar/")


@pytest.mark.asyncio
async def test_reserve(dxpchain, default_account):
    amount = await Amount("10 TEST")
    await dxpchain.reserve(amount, account=default_account)


@pytest.mark.asyncio
async def test_create_asset(dxpchain, default_account, smarttoken):
    asset = smarttoken
    assert asset.is_smarttoken


@pytest.mark.asyncio
async def test_create_benefactor(testbenefactor, default_account):
    w = await Benefactors(default_account)
    assert len(w) > 0


@pytest.mark.asyncio
async def test_fund_fee_pool(dxpchain, default_account, smarttoken):
    await dxpchain.fund_fee_pool(smarttoken.symbol, 100.0, account=default_account)


@pytest.mark.asyncio
async def test_create_dxpcore_member(dxpchain, ltm_account):
    await dxpchain.create_dxpcore_member(account=ltm_account)


@pytest.mark.asyncio
async def test_account_whitelist(dxpchain, default_account):
    await dxpchain.account_whitelist("init1", account=default_account)


@pytest.mark.asyncio
async def test_bid_collateral(dxpchain, default_account, gs_smarttoken):
    asset = gs_smarttoken
    additional_collateral = await Amount("1000 TEST")
    debt_covered = await Amount(10, asset)
    await dxpchain.bid_collateral(
        additional_collateral, debt_covered, account=default_account
    )


@pytest.mark.asyncio
async def test_asset_settle(dxpchain, default_account, smarttoken):
    asset = smarttoken
    dex = Dex(blockchain_instance=dxpchain)
    to_borrow = await Amount(1000, asset)
    await dex.borrow(to_borrow, collateral_ratio=2.1, account=default_account)
    to_settle = await Amount(100, asset)
    await dxpchain.asset_settle(to_settle, account=default_account)


@pytest.mark.asyncio
async def test_htlc(dxpchain, default_account):
    """Test both htlc_create and htlc_redeem."""
    amount = await Amount("10 TEST")
    dxpchain.blocking = "head"
    tx = await dxpchain.htlc_create(
        amount, default_account, "foobar", account=default_account
    )
    htlc_id = tx["operation_results"][0][1]
    await dxpchain.htlc_redeem(htlc_id, "foobar", account=default_account)
    dxpchain.blocking = None


@pytest.mark.asyncio
async def test_subscribe_to_pending_transactions(dxpchain, default_account):
    await dxpchain.cancel_subscriptions()
    await dxpchain.subscribe_to_pending_transactions()

    # Generate an event
    await dxpchain.transfer("init1", 10, "TEST", memo="xxx", account=default_account)

    event_correct = False
    for _ in range(0, 6):
        event = await dxpchain.notifications.get()
        if event["params"][0] == 0:
            event_correct = True
            break
    assert event_correct


@pytest.mark.asyncio
async def test_subscribe_to_blocks(dxpchain):
    await dxpchain.cancel_subscriptions()
    await dxpchain.subscribe_to_blocks()
    event_correct = False
    for _ in range(0, 6):
        event = await dxpchain.notifications.get()
        if event["params"][0] == 2:
            event_correct = True
            break
    assert event_correct


@pytest.mark.asyncio
async def test_subscribe_to_accounts(dxpchain, default_account):
    await dxpchain.cancel_subscriptions()
    # Subscribe
    await dxpchain.subscribe_to_accounts([default_account])

    # Generate an event
    await dxpchain.transfer("init1", 10, "TEST", memo="xxx", account=default_account)

    # Check event
    event_correct = False
    for _ in range(0, 6):
        event = await dxpchain.notifications.get()
        if event["params"][0] == 1:
            event_correct = True
            break
    assert event_correct


@pytest.mark.asyncio
async def test_subscribe_to_market(dxpchain, assets, default_account):
    await dxpchain.cancel_subscriptions()
    await asyncio.sleep(1.1)
    market = await Market("TEST/USD")
    await dxpchain.subscribe_to_market(market, event_id=4)

    # Generate an event
    await market.sell(1, 1, account=default_account)

    # Check event
    event_correct = False
    for _ in range(0, 10):
        event = await dxpchain.notifications.get()
        log.debug("getting event")
        if event["params"][0] == 4:
            event_correct = True
            break
    assert event_correct


@pytest.mark.asyncio
async def test_double_connect(dxpchain_testnet):
    from dxpchain.aio import DxpChain

    dxpchain = DxpChain(
        node="ws://127.0.0.1:{}".format(dxpchain_testnet.service_port), num_retries=-1
    )
    await dxpchain.connect()
    await dxpchain.connect()
