# -*- coding: utf-8 -*-
from .account import Account
from .blockchainobject import BlockchainObject
from .instance import BlockchainInstance
from graphenecommon.benefactor import Benefactor as GrapheneBenefactor, Benefactors as GrapheneBenefactors


@BlockchainInstance.inject
class Benefactor(GrapheneBenefactor):
    """
    Read data about a benefactor in the chain.

    :param str id: id of the benefactor
    :param dxpchain blockchain_instance: DxpChain() instance to use when
        accesing a RPC
    """

    def define_classes(self):
        self.account_class = Account
        self.type_id = 14


@BlockchainInstance.inject
class Benefactors(GrapheneBenefactors):
    """
    Obtain a list of benefactors for an account.

    :param str account_name/id: Name/id of the account (optional)
    :param dxpchain blockchain_instance: DxpChain() instance to use when
        accesing a RPC
    """

    def define_classes(self):
        self.account_class = Account
        self.benefactor_class = Benefactor
