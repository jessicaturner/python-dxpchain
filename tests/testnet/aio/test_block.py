# -*- coding: utf-8 -*-
import asyncio
import pytest
import logging

from dxpchain.aio.block import Block

log = logging.getLogger("grapheneapi")
log.setLevel(logging.DEBUG)


@pytest.mark.asyncio
async def test_aio_block(dxpchain):
    # Wait for block
    await asyncio.sleep(1)
    block = await Block(1, blockchain_instance=dxpchain)
    assert block["blockproducer"].startswith("1.6.")
    # Tests __contains__
    assert "blockproducer" in block
