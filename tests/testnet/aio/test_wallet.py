# -*- coding: utf-8 -*-
import pytest
import logging

from dxpchain.aio.asset import Asset
from dxpchain.aio.account import Account

log = logging.getLogger("grapheneapi")
log.setLevel(logging.DEBUG)


@pytest.mark.asyncio
async def test_aio_wallet_key(dxpchain, default_account):
    """Check whether wallet contains key for default account."""
    a = await Account(default_account, blockchain_instance=dxpchain)
    assert a["id"] in await dxpchain.wallet.getAccounts()
