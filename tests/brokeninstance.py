# -*- coding: utf-8 -*-
class BrokenDxpchainInstance:
    def __init__(self, *args, **kwargs):
        pass

    def __getattr__(self, name):
        raise ValueError("Attempting to use BrokenDxpchainInstance")


class DxpchainIsolator(object):
    enabled = False

    @classmethod
    def enable(self):
        if not self.enabled:
            from dxpchain.instance import set_shared_dxpchain_instance

            broken = BrokenDxpchainInstance()
            set_shared_dxpchain_instance(broken)
            self.enabled = True
