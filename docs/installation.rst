************
Installation
************

Installation
############

Install with `pip`:

::

    $ sudo apt-get install libffi-dev libssl-dev python3-dev
    $ pip3 install dxpchain

Manual installation:

::

    $ git clone https://github.com/xeroc/python-dxpchain/
    $ cd python-dxpchain
    $ python3 setup.py install --user

Upgrade
#######

::

   $ pip3 install dxpchain --user --upgrade
