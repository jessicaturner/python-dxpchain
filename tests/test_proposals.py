# -*- coding: utf-8 -*-
import unittest
from pprint import pprint
from dxpchain import DxpChain
from dxpchainbase.operationids import getOperationNameForId
from dxpchain.instance import set_shared_dxpchain_instance
from .fixtures import fixture_data, dxpchain


class Testcases(unittest.TestCase):
    def setUp(self):
        fixture_data()

    def test_finalizeOps_proposal(self):
        # proposal = dxpchain.new_proposal(dxpchain.tx())
        proposal = dxpchain.proposal()
        dxpchain.transfer("init1", 1, "DXP", append_to=proposal)
        tx = dxpchain.tx().json()  # default tx buffer
        ops = tx["operations"]
        self.assertEqual(len(ops), 1)
        self.assertEqual(getOperationNameForId(ops[0][0]), "proposal_create")
        prop = ops[0][1]
        self.assertEqual(len(prop["proposed_ops"]), 1)
        self.assertEqual(
            getOperationNameForId(prop["proposed_ops"][0]["op"][0]), "transfer"
        )

    def test_finalizeOps_proposal2(self):
        proposal = dxpchain.new_proposal()
        # proposal = dxpchain.proposal()
        dxpchain.transfer("init1", 1, "DXP", append_to=proposal)
        tx = dxpchain.tx().json()  # default tx buffer
        ops = tx["operations"]
        self.assertEqual(len(ops), 1)
        self.assertEqual(getOperationNameForId(ops[0][0]), "proposal_create")
        prop = ops[0][1]
        self.assertEqual(len(prop["proposed_ops"]), 1)
        self.assertEqual(
            getOperationNameForId(prop["proposed_ops"][0]["op"][0]), "transfer"
        )

    def test_finalizeOps_combined_proposal(self):
        parent = dxpchain.new_tx()
        proposal = dxpchain.new_proposal(parent)
        dxpchain.transfer("init1", 1, "DXP", append_to=proposal)
        dxpchain.transfer("init1", 1, "DXP", append_to=parent)
        tx = parent.json()
        ops = tx["operations"]
        self.assertEqual(len(ops), 2)
        self.assertEqual(getOperationNameForId(ops[0][0]), "proposal_create")
        self.assertEqual(getOperationNameForId(ops[1][0]), "transfer")
        prop = ops[0][1]
        self.assertEqual(len(prop["proposed_ops"]), 1)
        self.assertEqual(
            getOperationNameForId(prop["proposed_ops"][0]["op"][0]), "transfer"
        )

    def test_finalizeOps_changeproposer_new(self):
        proposal = dxpchain.proposal(proposer="init5")
        dxpchain.transfer("init1", 1, "DXP", append_to=proposal)
        tx = dxpchain.tx().json()
        ops = tx["operations"]
        self.assertEqual(len(ops), 1)
        self.assertEqual(getOperationNameForId(ops[0][0]), "proposal_create")
        prop = ops[0][1]
        self.assertEqual(len(prop["proposed_ops"]), 1)
        self.assertEqual(prop["fee_paying_account"], "1.2.90747")
        self.assertEqual(
            getOperationNameForId(prop["proposed_ops"][0]["op"][0]), "transfer"
        )

    """
    def test_finalizeOps_changeproposer_legacy(self):
        dxpchain.proposer = "init5"
        tx = dxpchain.transfer("init1", 1, "DXP")
        ops = tx["operations"]
        self.assertEqual(len(ops), 1)
        self.assertEqual(
            getOperationNameForId(ops[0][0]),
            "proposal_create")
        prop = ops[0][1]
        self.assertEqual(len(prop["proposed_ops"]), 1)
        self.assertEqual(prop["fee_paying_account"], "1.2.11")
        self.assertEqual(
            getOperationNameForId(prop["proposed_ops"][0]["op"][0]),
            "transfer")
    """

    def test_new_proposals(self):
        p1 = dxpchain.new_proposal()
        p2 = dxpchain.new_proposal()
        self.assertIsNotNone(id(p1), id(p2))

    def test_new_txs(self):
        p1 = dxpchain.new_tx()
        p2 = dxpchain.new_tx()
        self.assertIsNotNone(id(p1), id(p2))
