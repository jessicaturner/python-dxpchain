# -*- coding: utf-8 -*-
from .account import Account
from .instance import BlockchainInstance
from graphenecommon.aio.blockproducer import (
    Blockproducer as GrapheneBlockproducer,
    Blockproducers as GrapheneBlockproducers,
)


@BlockchainInstance.inject
class Blockproducer(GrapheneBlockproducer):
    """
    Read data about a blockproducer in the chain.

    :param str account_name: Name of the blockproducer
    :param dxpchain blockchain_instance: DxpChain() instance to use when
           accesing a RPC
    """

    def define_classes(self):
        self.account_class = Account
        self.type_ids = [6, 2]


@BlockchainInstance.inject
class Blockproducers(GrapheneBlockproducers):
    """
    Obtain a list of **active** blockproducers and the current schedule.

    :param bool only_active: (False) Only return blockproducers that are
        actively producing blocks
    :param dxpchain blockchain_instance: DxpChain() instance to use when
        accesing a RPC
    """

    def define_classes(self):
        self.account_class = Account
        self.blockproducer_class = Blockproducer
