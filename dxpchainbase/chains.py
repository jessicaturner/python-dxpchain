# -*- coding: utf-8 -*-
known_chains = {
    "DXP": {
        "chain_id": "23ae9e6b52450ad617b9f604bbb4080a6aa8fac56ccf46ed0413b6cca9eee7c3",
        "core_symbol": "DXP",
        "prefix": "DXP",
    },
    "TEST": {
        "chain_id": "23ae9e6b52450ad617b9f604bbb4080a6aa8fac56ccf46ed0413b6cca9eee7c3",
        "core_symbol": "TEST",
        "prefix": "TEST",
    },
}
