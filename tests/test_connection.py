# -*- coding: utf-8 -*-
import unittest
from dxpchain import DxpChain
from dxpchain.asset import Asset
from dxpchain.instance import set_shared_dxpchain_instance, SharedInstance
from dxpchain.blockchainobject import BlockchainObject

import logging

log = logging.getLogger()


class Testcases(unittest.TestCase):
    def test_default_connection(self):
        b1 = DxpChain("ws://localhost:11011", nobroadcast=True)
        set_shared_dxpchain_instance(b1)
        test = Asset("1.3.0", blockchain_instance=b1)
        # Needed to clear cache
        test.refresh()

        """
        b2 = DxpChain("wss://node.dxpchain.eu", nobroadcast=True)
        set_shared_dxpchain_instance(b2)
        dxp = Asset("1.3.0", blockchain_instance=b2)
        # Needed to clear cache
        dxp.refresh()

        self.assertEqual(test["symbol"], "TEST")
        self.assertEqual(dxp["symbol"], "DXP")
        """

    def test_default_connection2(self):
        b1 = DxpChain("ws://localhost:11011", nobroadcast=True)
        test = Asset("1.3.0", blockchain_instance=b1)
        test.refresh()

        """
        b2 = DxpChain("wss://node.dxpchain.eu", nobroadcast=True)
        dxp = Asset("1.3.0", blockchain_instance=b2)
        dxp.refresh()

        self.assertEqual(test["symbol"], "TEST")
        self.assertEqual(dxp["symbol"], "DXP")
        """
